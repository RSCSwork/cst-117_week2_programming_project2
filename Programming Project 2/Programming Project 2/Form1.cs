﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Project_2
{
    public partial class Define : Form
    {
        public Define()
        {
            InitializeComponent();
        }

        private void DefineBtn_Click(object sender, EventArgs e)
        {
            string selectedItem = listBox1.Items[listBox1.SelectedIndex].ToString();

            if (selectedItem == "Misnomer"){
                if (definition1RdBtn.Checked){
                    definitionOutputLbl.Text = "the misnaming of a person in a legal instrument";
                    if(pronunciationChkBx.Checked && synonymChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "miss-NOH-mer";
                        synonymOutputLbl.Text = "misnaming";
                    }else if (pronunciationChkBx.Checked){
                        pronunciationOutputLbl.Text = "miss-NOH-mer";
                        synonymOutputLbl.Text = "";
                    }else if (synonymChkBx.Checked){
                        synonymOutputLbl.Text = "misnaming";
                        pronunciationOutputLbl.Text = "";
                    }
                    else
                    {
                        pronunciationOutputLbl.Text = "";
                        synonymOutputLbl.Text = "";
                    }
                }
                else if(definition2RdBtn.Checked){
                    definitionOutputLbl.Text = "a use of a wrong or inappropriate name";
                    if (pronunciationChkBx.Checked && synonymChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "miss-NOH-mer";
                        synonymOutputLbl.Text = "misnaming";
                    }
                    else if (pronunciationChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "miss-NOH-mer";
                        synonymOutputLbl.Text = "";
                    }
                    else if (synonymChkBx.Checked)
                    {
                        synonymOutputLbl.Text = "misnaming";
                        pronunciationOutputLbl.Text = "";
                    }
                    else
                    {
                        pronunciationOutputLbl.Text = "";
                        synonymOutputLbl.Text = "";
                    }
                }
            }
            else if(selectedItem == "Exuberance"){
                if (definition1RdBtn.Checked){
                    definitionOutputLbl.Text = "the quality or state of being exuberant";
                    if (pronunciationChkBx.Checked && synonymChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "ex-u-ber-ance";
                        synonymOutputLbl.Text = "peppiness";
                    }
                    else if (pronunciationChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "ex-u-ber-ance";
                        synonymOutputLbl.Text = "";
                    }
                    else if (synonymChkBx.Checked)
                    {
                        synonymOutputLbl.Text = "peppiness";
                        pronunciationOutputLbl.Text = "";
                    }
                    else
                    {
                        pronunciationOutputLbl.Text = "";
                        synonymOutputLbl.Text = "";
                    }
                }
                else if (definition2RdBtn.Checked)
                {
                    definitionOutputLbl.Text = "an exuberant act or expression";
                    if (pronunciationChkBx.Checked && synonymChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "ex-u-ber-ance";
                        synonymOutputLbl.Text = "peppiness";
                    }
                    else if (pronunciationChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "ex-u-ber-ance";
                        synonymOutputLbl.Text = "";
                    }
                    else if (synonymChkBx.Checked)
                    {
                        synonymOutputLbl.Text = "peppiness";
                        pronunciationOutputLbl.Text = "";
                    }
                    else
                    {
                        pronunciationOutputLbl.Text = "";
                        synonymOutputLbl.Text = "";
                    }
                }
            }//ends exuberance block
            else if(selectedItem == "Encomiums"){
                if (definition1RdBtn.Checked)
                {
                    definitionOutputLbl.Text = "glowing and warmly enthusiastic praise";
                    if (pronunciationChkBx.Checked && synonymChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "en-co-mi-um";
                        synonymOutputLbl.Text = "accolade";
                    }
                    else if (pronunciationChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "en-co-mi-um";
                        synonymOutputLbl.Text = "";
                    }
                    else if (synonymChkBx.Checked)
                    {
                        synonymOutputLbl.Text = "accolade";
                        pronunciationOutputLbl.Text = "";
                    }
                    else
                    {
                        pronunciationOutputLbl.Text = "";
                        synonymOutputLbl.Text = "";
                    }
                }
                else if (definition2RdBtn.Checked)
                {
                    definitionOutputLbl.Text = "also : an expression of this";
                    if (pronunciationChkBx.Checked && synonymChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "en-co-mi-um";
                        synonymOutputLbl.Text = "accolade";
                    }
                    else if (pronunciationChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "en-co-mi-um";
                        synonymOutputLbl.Text = "";
                    }
                    else if (synonymChkBx.Checked)
                    {
                        synonymOutputLbl.Text = "accolade";
                        pronunciationOutputLbl.Text = "";
                    }
                    else
                    {
                        pronunciationOutputLbl.Text = "";
                        synonymOutputLbl.Text = "";
                    }
                }
            }//ends encomium block
            else if(selectedItem == "Astute"){
                if (definition1RdBtn.Checked)
                {
                    definitionOutputLbl.Text = "having or showing shrewdness and an ability to notice and understand things clearly";
                    if (pronunciationChkBx.Checked && synonymChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "as-tute";
                        synonymOutputLbl.Text = "canny";
                    }
                    else if (pronunciationChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "as-tute";
                        synonymOutputLbl.Text = "";
                    }
                    else if (synonymChkBx.Checked)
                    {
                        synonymOutputLbl.Text = "canny";
                        pronunciationOutputLbl.Text = "";
                    }
                    else
                    {
                        pronunciationOutputLbl.Text = "";
                        synonymOutputLbl.Text = "";
                    }
                }
                else if (definition2RdBtn.Checked)
                {
                    definitionOutputLbl.Text = ": mentally sharp or clever";
                    if (pronunciationChkBx.Checked && synonymChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "as-tute";
                        synonymOutputLbl.Text = "canny";
                    }
                    else if (pronunciationChkBx.Checked)
                    {
                        pronunciationOutputLbl.Text = "as-tute";
                        synonymOutputLbl.Text = "";
                    }
                    else if (synonymChkBx.Checked)
                    {
                        synonymOutputLbl.Text = "canny";
                        pronunciationOutputLbl.Text = "";
                    }
                    else
                    {
                        pronunciationOutputLbl.Text = "";
                        synonymOutputLbl.Text = "";
                    }
                }
            }//ends if else tree
        }//ends define btn

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            definitionOutputLbl.Text = "";
            pronunciationOutputLbl.Text = "";
            synonymOutputLbl.Text = "";
        }//ends clear btn
    }//ends class define
}// namespace
