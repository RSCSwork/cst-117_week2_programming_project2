﻿namespace Programming_Project_2
{
    partial class Define
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.radioBtnGroup = new System.Windows.Forms.GroupBox();
            this.definition2RdBtn = new System.Windows.Forms.RadioButton();
            this.definition1RdBtn = new System.Windows.Forms.RadioButton();
            this.checkBxGroup = new System.Windows.Forms.GroupBox();
            this.synonymChkBx = new System.Windows.Forms.CheckBox();
            this.pronunciationChkBx = new System.Windows.Forms.CheckBox();
            this.defineBtn = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.definitionLbl = new System.Windows.Forms.Label();
            this.pronunciationLbl = new System.Windows.Forms.Label();
            this.synonymLbl = new System.Windows.Forms.Label();
            this.definitionOutputLbl = new System.Windows.Forms.Label();
            this.pronunciationOutputLbl = new System.Windows.Forms.Label();
            this.synonymOutputLbl = new System.Windows.Forms.Label();
            this.selectLbl = new System.Windows.Forms.Label();
            this.radioBtnGroup.SuspendLayout();
            this.checkBxGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Items.AddRange(new object[] {
            "Misnomer",
            "Exuberance",
            "Encomiums",
            "Astute"});
            this.listBox1.Location = new System.Drawing.Point(46, 34);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(138, 104);
            this.listBox1.TabIndex = 0;
            // 
            // radioBtnGroup
            // 
            this.radioBtnGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radioBtnGroup.Controls.Add(this.definition2RdBtn);
            this.radioBtnGroup.Controls.Add(this.definition1RdBtn);
            this.radioBtnGroup.Location = new System.Drawing.Point(46, 144);
            this.radioBtnGroup.Name = "radioBtnGroup";
            this.radioBtnGroup.Size = new System.Drawing.Size(138, 138);
            this.radioBtnGroup.TabIndex = 1;
            this.radioBtnGroup.TabStop = false;
            this.radioBtnGroup.Text = "Definition Selection";
            this.radioBtnGroup.UseWaitCursor = true;
            // 
            // definition2RdBtn
            // 
            this.definition2RdBtn.AutoSize = true;
            this.definition2RdBtn.Location = new System.Drawing.Point(6, 95);
            this.definition2RdBtn.Name = "definition2RdBtn";
            this.definition2RdBtn.Size = new System.Drawing.Size(114, 24);
            this.definition2RdBtn.TabIndex = 1;
            this.definition2RdBtn.TabStop = true;
            this.definition2RdBtn.Text = "Definition 2";
            this.definition2RdBtn.UseVisualStyleBackColor = true;
            this.definition2RdBtn.UseWaitCursor = true;
            // 
            // definition1RdBtn
            // 
            this.definition1RdBtn.AutoSize = true;
            this.definition1RdBtn.Location = new System.Drawing.Point(6, 54);
            this.definition1RdBtn.Name = "definition1RdBtn";
            this.definition1RdBtn.Size = new System.Drawing.Size(114, 24);
            this.definition1RdBtn.TabIndex = 0;
            this.definition1RdBtn.TabStop = true;
            this.definition1RdBtn.Text = "Definition 1";
            this.definition1RdBtn.UseVisualStyleBackColor = true;
            this.definition1RdBtn.UseWaitCursor = true;
            // 
            // checkBxGroup
            // 
            this.checkBxGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBxGroup.Controls.Add(this.synonymChkBx);
            this.checkBxGroup.Controls.Add(this.pronunciationChkBx);
            this.checkBxGroup.Location = new System.Drawing.Point(46, 300);
            this.checkBxGroup.Name = "checkBxGroup";
            this.checkBxGroup.Size = new System.Drawing.Size(138, 138);
            this.checkBxGroup.TabIndex = 2;
            this.checkBxGroup.TabStop = false;
            this.checkBxGroup.Text = "Optional Information";
            // 
            // synonymChkBx
            // 
            this.synonymChkBx.AutoSize = true;
            this.synonymChkBx.Location = new System.Drawing.Point(6, 97);
            this.synonymChkBx.Name = "synonymChkBx";
            this.synonymChkBx.Size = new System.Drawing.Size(100, 24);
            this.synonymChkBx.TabIndex = 1;
            this.synonymChkBx.Text = "Synonym";
            this.synonymChkBx.UseVisualStyleBackColor = true;
            // 
            // pronunciationChkBx
            // 
            this.pronunciationChkBx.AutoSize = true;
            this.pronunciationChkBx.Location = new System.Drawing.Point(6, 52);
            this.pronunciationChkBx.Name = "pronunciationChkBx";
            this.pronunciationChkBx.Size = new System.Drawing.Size(132, 24);
            this.pronunciationChkBx.TabIndex = 0;
            this.pronunciationChkBx.Text = "Pronunciation";
            this.pronunciationChkBx.UseVisualStyleBackColor = true;
            // 
            // defineBtn
            // 
            this.defineBtn.Location = new System.Drawing.Point(265, 383);
            this.defineBtn.MinimumSize = new System.Drawing.Size(200, 50);
            this.defineBtn.Name = "defineBtn";
            this.defineBtn.Size = new System.Drawing.Size(200, 50);
            this.defineBtn.TabIndex = 3;
            this.defineBtn.Text = "Define";
            this.defineBtn.UseVisualStyleBackColor = true;
            this.defineBtn.Click += new System.EventHandler(this.DefineBtn_Click);
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(496, 383);
            this.clearBtn.MinimumSize = new System.Drawing.Size(200, 50);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(200, 50);
            this.clearBtn.TabIndex = 4;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // definitionLbl
            // 
            this.definitionLbl.AutoSize = true;
            this.definitionLbl.Location = new System.Drawing.Point(222, 84);
            this.definitionLbl.Name = "definitionLbl";
            this.definitionLbl.Size = new System.Drawing.Size(80, 20);
            this.definitionLbl.TabIndex = 5;
            this.definitionLbl.Text = "Definition:";
            // 
            // pronunciationLbl
            // 
            this.pronunciationLbl.AutoSize = true;
            this.pronunciationLbl.Location = new System.Drawing.Point(222, 152);
            this.pronunciationLbl.Name = "pronunciationLbl";
            this.pronunciationLbl.Size = new System.Drawing.Size(110, 20);
            this.pronunciationLbl.TabIndex = 6;
            this.pronunciationLbl.Text = "Pronunciation:";
            // 
            // synonymLbl
            // 
            this.synonymLbl.AutoSize = true;
            this.synonymLbl.Location = new System.Drawing.Point(222, 235);
            this.synonymLbl.Name = "synonymLbl";
            this.synonymLbl.Size = new System.Drawing.Size(78, 20);
            this.synonymLbl.TabIndex = 7;
            this.synonymLbl.Text = "Synonym:";
            // 
            // definitionOutputLbl
            // 
            this.definitionOutputLbl.AutoSize = true;
            this.definitionOutputLbl.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.definitionOutputLbl.Location = new System.Drawing.Point(308, 84);
            this.definitionOutputLbl.Name = "definitionOutputLbl";
            this.definitionOutputLbl.Size = new System.Drawing.Size(9, 20);
            this.definitionOutputLbl.TabIndex = 8;
            this.definitionOutputLbl.Text = "\r\n";
            this.definitionOutputLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pronunciationOutputLbl
            // 
            this.pronunciationOutputLbl.AutoSize = true;
            this.pronunciationOutputLbl.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.pronunciationOutputLbl.Location = new System.Drawing.Point(338, 152);
            this.pronunciationOutputLbl.Name = "pronunciationOutputLbl";
            this.pronunciationOutputLbl.Size = new System.Drawing.Size(0, 20);
            this.pronunciationOutputLbl.TabIndex = 9;
            this.pronunciationOutputLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // synonymOutputLbl
            // 
            this.synonymOutputLbl.AutoSize = true;
            this.synonymOutputLbl.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.synonymOutputLbl.Location = new System.Drawing.Point(338, 235);
            this.synonymOutputLbl.Name = "synonymOutputLbl";
            this.synonymOutputLbl.Size = new System.Drawing.Size(0, 20);
            this.synonymOutputLbl.TabIndex = 10;
            this.synonymOutputLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // selectLbl
            // 
            this.selectLbl.AutoSize = true;
            this.selectLbl.Location = new System.Drawing.Point(30, 11);
            this.selectLbl.Name = "selectLbl";
            this.selectLbl.Size = new System.Drawing.Size(179, 20);
            this.selectLbl.TabIndex = 11;
            this.selectLbl.Text = "Select a word of the day";
            // 
            // Define
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 450);
            this.Controls.Add(this.selectLbl);
            this.Controls.Add(this.synonymOutputLbl);
            this.Controls.Add(this.pronunciationOutputLbl);
            this.Controls.Add(this.definitionOutputLbl);
            this.Controls.Add(this.synonymLbl);
            this.Controls.Add(this.pronunciationLbl);
            this.Controls.Add(this.definitionLbl);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.defineBtn);
            this.Controls.Add(this.checkBxGroup);
            this.Controls.Add(this.radioBtnGroup);
            this.Controls.Add(this.listBox1);
            this.Name = "Define";
            this.Text = "Word of the Day Definer ";
            this.radioBtnGroup.ResumeLayout(false);
            this.radioBtnGroup.PerformLayout();
            this.checkBxGroup.ResumeLayout(false);
            this.checkBxGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox radioBtnGroup;
        private System.Windows.Forms.RadioButton definition1RdBtn;
        private System.Windows.Forms.RadioButton definition2RdBtn;
        private System.Windows.Forms.GroupBox checkBxGroup;
        private System.Windows.Forms.CheckBox synonymChkBx;
        private System.Windows.Forms.CheckBox pronunciationChkBx;
        private System.Windows.Forms.Button defineBtn;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Label definitionLbl;
        private System.Windows.Forms.Label pronunciationLbl;
        private System.Windows.Forms.Label synonymLbl;
        private System.Windows.Forms.Label definitionOutputLbl;
        private System.Windows.Forms.Label pronunciationOutputLbl;
        private System.Windows.Forms.Label synonymOutputLbl;
        private System.Windows.Forms.Label selectLbl;
    }
}

